import main.FileParser;
import main.WordProblem;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

public class FileParserTest {

    FileParser fp = new FileParser();
    @Test
    public void parserOpensFileTest() throws FileNotFoundException {
        fp.parseFile(new File("src/test/resources/input.txt"));
    }

    @Test
    public void parserGeneratesCorrectWordGridTest() throws FileNotFoundException {
        WordProblem wp = fp.parseFile(new File("src/test/resources/input.txt"));

        char[][] testGrid = {
                {'H','A','S','D','F'},
                {'G','E','Y','B','H'},
                {'J','K','L','Z','X'},
                {'C','V','B','L','N'},
                {'G','O','O','D','O'}
        };

        assertTrue(Arrays.deepEquals(testGrid, wp.getWordGrid()));
    }

    @Test
    public void parserGeneratesCorrectWordListTest() throws FileNotFoundException{
        WordProblem wp = fp.parseFile(new File("src/test/resources/input.txt"));

        ArrayList testList = new ArrayList(Arrays.asList(new String[]{"HELLO", "GOOD", "BYE"}));

        System.out.println(testList.toString());
        assertTrue(testList.equals(wp.getWordList()));
    }

}
