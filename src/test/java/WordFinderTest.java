import jdk.jfr.StackTrace;
import main.WordFinder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WordFinderTest {

    private WordFinder wf = new WordFinder();

    @Test
    public void findHorizontalWordInOneRowGridTest(){
        char[][] wordGrid = {
                {'a', 't'}
        };
        var word = "at";
        var result = wf.findWord(wordGrid, word);

        assertEquals("at 0:0 0:1", result.toString());
    }

    @Test
    public void findHorizontalWordInTwoRowGridTest(){
        char[][] wordGrid = {
                {'a', 't'},
                {'b', 'y'}
        };
        var word = "by";
        var result = wf.findWord(wordGrid, word);

        assertEquals("by 1:0 1:1", result.toString());
    }

    @Test
    public void findReverseHorizontalWordInTwoRowGridTest(){
        char[][] wordGrid = {
                {'t', 'a'},
                {'b', 'y'}
        };
        var word = "at";

        var result = wf.findWord(wordGrid, word);

        assertEquals("at 0:1 0:0", result.toString());

    }

    @Test
    public void findVerticalWordInOneColumnGridTest(){
        char[][] wordGrid = {
                {'a'},
                {'t'}
        };
        var word = "at";
        var result = wf.findWord(wordGrid, word);

        assertEquals("at 0:0 1:0", result.toString());
    }

    @Test
    public void findVerticalWordInTwoColumnGridTest(){
        char[][] wordGrid = {
                {'a', 'b'},
                {'t', 'y'}
        };
        var word = "by";
        var result = wf.findWord(wordGrid, word);

        assertEquals("by 0:1 1:1", result.toString());

    }

    @Test
    public void findReverseVerticalWordInTowColumnGridTest(){
        char[][] wordGrid = {
                {'t', 'y'},
                {'a', 'b'}
        };
        var word = "by";
        var result = wf.findWord(wordGrid, word);

        assertEquals("by 1:1 0:1", result.toString());

    }

    @Test
    public void findForwardDiagonalWordInTwoColumnGridTest(){
        char[][] wordGrid = {
                {'a', 'b'},
                {'y', 't'}
            };
        var word = "at";
        var result = wf.findWord(wordGrid, word);

        assertEquals("at 0:0 1:1", result.toString());
    }

    @Test
    public void findReverseForwardDiagonalWordInTwoColumnGridTest(){
        char[][] wordGrid = {
                {'t', 'b'},
                {'y', 'a'}
        };
        var word = "at";
        var result = wf.findWord(wordGrid, word);

        assertEquals("at 1:1 0:0", result.toString());
    }

    @Test
    public void findBackwardsDiagonalWordInTwoColumnGridTest(){
        char[][] wordGrid = {
                {'t', 'b'},
                {'y', 'a'}
        };
        var word = "by";
        var result = wf.findWord(wordGrid, word);

        assertEquals("by 0:1 1:0", result.toString());
    }

    @Test
    public void findReverseBackwardsDiagonalWordInTwoColumnGridTest(){
        char[][] wordGrid = {
                {'t', 'y'},
                {'b', 'a'}
        };
        var word = "by";
        var result = wf.findWord(wordGrid, word);

        assertEquals("by 1:0 0:1", result.toString());
    }

    @Test
    public void findWordWithSpaceInFourColumnGridTest(){
        char[][] wordGrid = {
                {'t','y','b','a'},
                {'t','b','c','d'},
                {'t','o','o','n'}
        };
        var word = "to on";
        var result = wf.findWord(wordGrid, word);

        assertEquals("toon 2:0 2:3", result.toString());
    }
}
