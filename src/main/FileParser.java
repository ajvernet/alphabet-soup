package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class FileParser {

    public static WordProblem parseFile(File inputFile) throws FileNotFoundException {
        Scanner scanner = new Scanner(inputFile);

        // get grid dimensions
        String dimensionsLine = scanner.next();
        var dimensions = dimensionsLine.split("x");
        var rows = Integer.valueOf(dimensions[0]);
        var cols = Integer.valueOf(dimensions[1]);

        // propagate grid
        char[][] wordGrid = new char[rows][cols];

        scanner.nextLine();
        for (int i = 0;i < rows ;i++){
            var gridRow = scanner.nextLine().replaceAll(" ", "").toCharArray();
            wordGrid[i] = gridRow;
        }

        // generate list of words
        var wordList = new ArrayList<String>();
        while(scanner.hasNext()){
            wordList.add(scanner.nextLine());
        }

        return new WordProblem(wordGrid, wordList);
    }

}
