package main;

public class WordData
{
    private final String word;
    private final int startY;
    private final int startX;
    private final int endY;
    private final int endX;


    public WordData(String word, int startY, int startX, int endY, int endX) {
        this.word = word;
        this.startY = startY;
        this.startX = startX;
        this.endY = endY;
        this.endX = endX;
    }

    public String getWord() {
        return word;
    }

    public int getStartY() {
        return startY;
    }

    public int getStartX() {
        return startX;
    }

    public int getEndY() {
        return endY;
    }

    public int getEndX() {
        return endX;
    }

    @Override
    public String toString() {
        return  word + " "
                + startY + ":" + startX + " "
                + endY + ":" + endX;
    }
}

