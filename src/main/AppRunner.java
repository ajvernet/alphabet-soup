package main;

import java.io.File;
import java.io.FileNotFoundException;

import static java.util.stream.Collectors.toList;

public class AppRunner {
    public static void main(String args[]) throws FileNotFoundException {
        File inputFile = new File(args[0]);

        FileParser fp = new FileParser();
        WordProblem wp = fp.parseFile(inputFile);
        WordFinder wordFinder = new WordFinder();

        var resultList =
                wp
                    .getWordList()
                    .stream()
                    .map(word -> wordFinder.findWord(wp.getWordGrid(), word))
                    .collect(toList());

        resultList.forEach(System.out::println);

    }
}
