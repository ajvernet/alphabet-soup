package main;

import java.util.ArrayList;
import java.util.Optional;

public class WordFinder {
    // should create list of these strings so that they only have to be created once
    public static WordData findWord(char[][] wordGrid, String word) {

        // remove all inner whitespace in word
        word = word.replace(" ", "");
        
        // first pass, check if word is contained in rows
        var rowData = createRowDataFromGrid(wordGrid);

        var wordInRows = getWordInRow(rowData, word);
        if(wordInRows.isPresent()){
            return wordInRows.get();
        }
        
        var reverseWordInRows = getWordInRowReverse(rowData, word);
        if (reverseWordInRows.isPresent()){
            return reverseWordInRows.get();
        }
        
        // second pass, check if word is contained in columns
        var columnData = createColumnDataFromGrid(wordGrid);
        
        var wordInColumn = getWordInColumn(columnData, word);
        if (wordInColumn.isPresent()){
            return wordInColumn.get();
        }
        var reverseWordInColumn = getWordInColumnReverse(columnData, word);
        if (reverseWordInColumn.isPresent()){
            return reverseWordInColumn.get();
        }

        // third pass, check if word is contained in forward diagonals
        var forwardDiagonalData = createDiagonalForwardDataFromGrid(wordGrid);
        var wordInForwardDiagonal = getWordInForwardDiagonal(forwardDiagonalData, word);
        if (wordInForwardDiagonal.isPresent()) {
            return wordInForwardDiagonal.get();
        }

        var reverseWordInForwardDiagonal = getWordInForwardDiagonalReverse(forwardDiagonalData, word);
        if (reverseWordInForwardDiagonal.isPresent()){
            return reverseWordInForwardDiagonal.get();
        }

        // fourth pass, check if word is contained in backwards diagonals
        var backwardsDiagonalData = createDiagonalBackwardsDataFromGrid(wordGrid);
        var wordInBackwardsDiagonal = getWordInBackwardsDiagonal(backwardsDiagonalData, word);
        if (wordInBackwardsDiagonal.isPresent()){
            return wordInBackwardsDiagonal.get();
        }
        
        var reverseWordInBackwardsDiagonal = getWordInBackwardsDiagonalReverse(backwardsDiagonalData, word);
        if (reverseWordInBackwardsDiagonal.isPresent()){
            return reverseWordInBackwardsDiagonal.get();
        }

        return new WordData("word not found", 0,0,0,0);
    }

    private static ArrayList<WordData> createDiagonalBackwardsDataFromGrid(char[][] wordGrid) {
        var backwardsDiagonalDataList = new ArrayList<WordData>();

        int lengthLimit = wordGrid[0].length;

        for (int i = 0;i < wordGrid.length;i++){            // loop through rows


        for (int j = wordGrid[0].length - 1; j > -1; j-- ) {   // loop backwards through columns

            var diagonalString = new StringBuilder();
            // WordData indexes used for diagonals
            int startY = i;
            int startX = j;
            int endY = 0;
            int endX = 0;

            int a = i;
            for (int k = j; k > -1; k--) {                     // for each column, append char in wordGrid[row][col], then wordGrid[row+1][col-1]
                diagonalString.append(wordGrid[a][k]);
                endY = a;
                endX = k;
                if (a == wordGrid.length - 1){
                    break;
                }
                a++;
            }

            backwardsDiagonalDataList.add(new WordData(diagonalString.toString(), startY, startX, endY, endX));

             lengthLimit--;
            }
        }

        return backwardsDiagonalDataList;
    }

    private static ArrayList<WordData> createDiagonalForwardDataFromGrid(char[][] wordGrid){
        var forwardDiagonalDataList = new ArrayList<WordData>();

        int lengthLimit = wordGrid[0].length;

        for (int i = 0; i < wordGrid.length; i++){    // loop through rows

            var diagonalString = new StringBuilder();


            for (int j = 0;j < wordGrid[0].length; j++ ){  // loop through columns

                // WordData indexes used for diagonals
                int startY = 0;
                int startX = i;
                int endY = 0;
                int endX = 0;

                int a = i;
                for (int k = j; k < lengthLimit; k++) {   // for each column, append char in wordGrid[row][col], then wordGrid[row+1][col+1]
                    diagonalString.append(wordGrid[a][k]);
                    endY = k;
                    endX = a;
                    a++;
                }

                forwardDiagonalDataList.add(new WordData(diagonalString.toString(), startY, startX, endY, endX));
            }

            lengthLimit--;

        }

        return forwardDiagonalDataList;
    }

    private static ArrayList<WordData> createRowDataFromGrid(char[][] wordGrid) {

        var rowDataList = new ArrayList<WordData>();
        for (int i = 0; i < wordGrid.length; i++) {

            // WordData indexes used for rows
            int startY = i;
            int startX = 0;
            int endY = i;
            int endX = 0;


            for (int j = 0; j < wordGrid[i].length; j++){
                rowDataList.add(new WordData(getRowString(wordGrid, i), startY, startX, endY, endX));
            }
        }
        return rowDataList;
    }

    private static ArrayList<WordData> createColumnDataFromGrid(char[][] wordGrid) {


        var colDataList = new ArrayList<WordData>();
        for (int i = 0; i < wordGrid[0].length; i++) {

            // WordData indexes used for columns
            int startY = 0;
            int startX = i;
            int endY = 0;
            int endX = i;

            colDataList.add(new WordData(getColumnString(wordGrid, i), startY, startX, endY, endX));
        }
        return colDataList;
    }

    private static String getColumnString(char[][] wordGrid, int columnIndex){
        StringBuffer verticalStringBuffer = new StringBuffer();
        for (int j = 0; j < wordGrid.length; j++) {
            verticalStringBuffer.append(wordGrid[j][columnIndex]);
        }

        return new String(verticalStringBuffer);
    }

    private static String getRowString(char[][] wordGrid, int rowIndex){
        return new String(wordGrid[rowIndex]);
    }

    private static Optional<WordData> getWordInRow(ArrayList<WordData> rowsStrings, String word){
        return rowsStrings.stream()
                .filter(s -> s.getWord().contains(word))
                .map(s -> new WordData(
                        word, s.getStartY(), s.getWord().indexOf(word),
                        s.getEndY(), (s.getWord().indexOf(word) + word.length() - 1)
                ))
                .findFirst();
    }

    private static Optional<WordData> getWordInRowReverse(ArrayList<WordData> rowsStrings, String word){
        var reverseWord = new StringBuilder(word).reverse().toString();

        return rowsStrings.stream()
                .filter(s -> s.getWord().contains(reverseWord))
                .map(s -> new WordData(
                        word, s.getStartY(), s.getWord().indexOf(reverseWord)+ word.length() - 1,
                        s.getEndY(), (s.getWord().indexOf(reverseWord))
                ))
                .findFirst();
    }

    private static Optional<WordData> getWordInColumn(ArrayList<WordData> columnStrings, String word){
        return columnStrings.stream()
                .filter(s -> s.getWord().contains(word))
                .map(s -> new WordData(
                        word, s.getWord().indexOf(word), s.getStartX(),
                        s.getWord().indexOf(word) + word.length() - 1, s.getEndX()))
                .findFirst();
    }

    private static Optional<WordData> getWordInColumnReverse(ArrayList<WordData> columnData, String word){

        var reverseWord = new StringBuilder(word).reverse().toString();
        return columnData.stream()
                .filter(s -> s.getWord().contains(reverseWord))
                .map(s -> new WordData(
                        word, s.getWord().indexOf(reverseWord) + reverseWord.length() - 1, s.getStartX(),
                        s.getWord().indexOf(reverseWord), s.getEndX()))
                .findFirst();
    }


    private static Optional<WordData> getWordInForwardDiagonal(ArrayList<WordData> forwardDiagonalData, String word){
        return  forwardDiagonalData.stream()
                .filter(s -> s.getWord().contains(word))
                .map(s -> new WordData(
                        word,
                        s.getWord().indexOf(word) + s.getStartY(),
                        s.getWord().indexOf(word) + s.getStartX(),
                        s.getWord().indexOf(word) + s.getStartY() + word.length() - 1,
                        s.getWord().indexOf(word) + s.getStartX() + word.length() - 1
                        ))
                .findFirst();
    }

    private static Optional<WordData> getWordInForwardDiagonalReverse(ArrayList<WordData> forwardDiagonalData, String word){

        var reverseWord = new StringBuilder(word).reverse().toString();
        return  forwardDiagonalData.stream()
                .filter(s -> s.getWord().contains(reverseWord))
                .map(s -> new WordData(
                        word,
                        s.getWord().indexOf(reverseWord) + s.getStartY() + word.length() - 1,
                        s.getWord().indexOf(reverseWord) + s.getStartX() + word.length() - 1,
                        s.getWord().indexOf(reverseWord) + s.getStartY(),
                        s.getWord().indexOf(reverseWord) + s.getStartX()

                ))
                .findFirst();
    }

    private static Optional<WordData> getWordInBackwardsDiagonal(ArrayList<WordData> backwardsDiagonalData, String word){
        return  backwardsDiagonalData.stream()
                .filter(s -> s.getWord().contains(word))
                .map(s -> new WordData(
                        word,
                        s.getWord().indexOf(word) + s.getStartY(),
                        s.getWord().indexOf(word) + s.getStartX(),
                        s.getWord().indexOf(word) + s.getStartY() + word.length() - 1,
                        s.getWord().indexOf(word) + s.getStartX() - (word.length() - 1)
                ))
                .findFirst();
    }

    private static Optional<WordData> getWordInBackwardsDiagonalReverse(ArrayList<WordData> backwardsDiagonalData, String word){
        var reverseWord = new StringBuilder(word).reverse().toString();

        return  backwardsDiagonalData.stream()
                .filter(s -> s.getWord().contains(reverseWord))
                .map(s -> new WordData(
                        word,
                        s.getStartY() + s.getWord().indexOf(reverseWord) +  (word.length() - 1),
                        s.getStartX() + s.getWord().indexOf(reverseWord) - (word.length() - 1),
                        s.getStartY() + s.getWord().indexOf(reverseWord),
                        s.getStartX() + s.getWord().indexOf(reverseWord)
                ))
                .findFirst();
    }
}
