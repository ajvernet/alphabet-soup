package main;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class WordProblem {

    private char[][] wordGrid;
    private ArrayList<String> wordList = new ArrayList<>();

    public WordProblem(char[][] wordGrid, ArrayList<String> words) {
        this.wordGrid = wordGrid;
        this.wordList = new ArrayList<String>(words);
    }

    public char[][] getWordGrid() {
        return wordGrid;
    }

    public ArrayList<String> getWordList() {
        return wordList;
    }

    @Override
    public String toString() {
        return "WordProblem{" +
                "wordGrid=" + Arrays.toString(wordGrid) +
                ", wordList=" + wordList +
                '}';
    }
}
